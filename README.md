# PyPet

A terminal based game inspired by Tamagochi written in Python 3.

# Dependencies

PyPet works without external modules! All you need is the Python 3 interpreter.

If you wish to compile the Python script into an ELF executable you'll need GCC and the C Python headers:

`sudo apt update && sudo apt install gcc python3-dev libpython3-dev -y`

# Usage & Installation

1. Clone the PyPet repository: `git clone https://gitlab.com/zombee_/pypet.git`

2. Make the PyPet script executable: `cd pypet && chmod +x ./src/pypet.py`

    2.1 (*Optional*) Compile it for better performance: `make build`

    2.2 (*Optional*) Install it for ease of access: `sudo make install`

3. Run the game: `./src/pypet.py`

    3.1 (*Notice*) If you compiled the script you can also run it with `./bin/pypet`

    3.2 (*Notice) If you compiled *and* installed the script you can run it from anywhere with `pypet`

Use the in-game `help` or `?` command to display available actions

Use the in-game `exit` command or spam **Ctrl+D** to exit

# Feedback

Found a bug? Want new features added? Please open an [issue tracker](https://gitlab.com/zombee_/pypet/-/issues) or send me an e-mail at z0mbee@protonmail.com
