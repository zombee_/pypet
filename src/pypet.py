#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import threading

class pet:
    photo = ["""/_/( o.o )^ <"""]

    # This stores information about the pet
    def __init__(self, name, hungry, weight, age):
        self.name = name
        self.hungry = hungry
        self.weight = weight
        self.age = age

    # This function feeds the pet
    def feed(self):
        if (self.hungry == True):
            print(f"Feeding {self.name}...")
            self.hungry = False
            self.weight += 1
            timer = threading.Timer(10.0, self.makehungry)
            timer.start()
        else:
            print(f"{self.name} is not hungry!")

    # This function makes the pet hungry 10 seconds after feeding it
    def makehungry(self):
        self.hungry = True
        self.weight -= 1

def main():
    cat = pet("Fluffy", True, 9.5, 5, "(=^o.o^=__")

    # Ask the user what to do
    while (True):
        action = input(">> ")

        if (action == "feed"):
            cat.feed()
        elif (action == "exit" or action == "quit"):
            print("Goodbye world!")
            exit()
        else:
            print(f"\"{action}\" is not a valid action!")

if __name__ == "__main__":
    main()
